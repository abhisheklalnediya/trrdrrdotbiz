from django.db import models
from trrdrrdotbiz.drrs.models import *

# Create your models here.

class Torrents(models.Model):
	Name = models.CharField("Name Of Torrent",max_length=200, default='No MetaData')
	Magnet = models.CharField("Magnet Url",max_length=2000,blank=True,null=True)
	TorrentFile = models.CharField("Magnet Url",max_length=200,blank=True,null=True)
	Status = models.CharField("Status", default='Just Added',max_length=100,blank=True,null=True)
	Progress = models.CharField("Progress Percentage",max_length=500,blank=True,null=True)
	drrs = models.ForeignKey(Drrs,blank=True,null=True)
	def __unicode__(self):
		return str(self.Name + ' : ' +self.Status)
	