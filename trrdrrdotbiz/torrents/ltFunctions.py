from trrdrrdotbiz.torrents.models import * ;
import libtorrent as lt
import time
import sys
import threading
import subprocess
import os
import zipfile
from ziper import zipMe

ses = lt.session()
state = None
#state = lt.bdecode(open(state_file, "rb").read())
ses.start_dht(state)
ses.add_dht_router("router.bittorrent.com", 6881)
ses.add_dht_router("router.utorrent.com", 6881)
ses.add_dht_router("router.bitcomet.com", 6881)
ses.listen_on(6881, 6891)
def transmissionHack(MagnetUrl):
	cmd = 'transmission-cli ' + MagnetUrl
	p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	linecount = 0
	
	for line in p.stdout.readlines():
		print line,
		if 'Saved' in line:
			print "Saved foundddddd"
			exit();
			break
	retval = p.wait()

def createZip( folder,zipFile ):
	zf = zipfile.ZipFile(zipFile, "w")
	for dirname, subdirs, files in os.walk(folder):
		zf.write(dirname)
		for filename in files:
			zf.write(os.path.join(dirname, filename))
			zf.close()

def validateTorrent(status):
	if status.total_wanted > 245 * 1024 * 1024:
		return False
	return True



TorrentThreads = []
def addTorrentMagnet(MagnetUrl, request):
	if request.user.is_authenticated():
		
		dbTorrent = Torrents(Magnet=MagnetUrl)
		dbInst = dbTorrent.save();
		
		
		
		save_path = '/home/abhisheklal/Downloads/lbtr/' + str(request.user) + '/' + str(dbTorrent.id) + '/'
		params = {
			'save_path': save_path,
			'storage_mode': lt.storage_mode_t(2),
			'paused': False,
			'auto_managed': True,
			'duplicate_is_error': False,
			'url': MagnetUrl
			}
		h = ses.add_torrent(params)
		#print 'starting', h.name()
		dbTorrent.Name = h.name()
		dbTorrent.save();
		#transmissionHack(MagnetUrl)
		s = h.status()
		
		freshFish = True
		
		while (not h.is_seed()):
			dbTorrent = Torrents.objects.get(pk = dbTorrent.pk)
			if str(dbTorrent.Status) == 'deleted':
				print 'deleted'
				ses.remove_torrent(h)
				return ''
			if freshFish:
				#print s.state
				if str(s.state) == 'downloading':
					if not validateTorrent(s):
						print 'Over Sized!!'
						dbTorrent.Status = 'Over Sized!!'
						dbTorrent.Progress = str(s.progress*100)
						dbTorrent.save()
						ses.remove_torrent(h)
						return ''
					freshFish = False
			s = h.status()
			dbTorrent.Status = s.state
			dbTorrent.Progress = str(s.progress*100);
			dbTorrent.save()
			#print dir(h)
			print s.total_wanted
			#print s.total_wanted_done
			
			
			#print s.error
			#print s.total_download
			
			time.sleep(1)
		#i = 0
		#print h.name(), 'complete'
		#while(1):
			#print s.state
			#print s.progress
			#i = i + 1
			#time.sleep(1)
		dbTorrent.Status = "Compressing"
		dbTorrent.Progress = str(100);
		dbTorrent.save()
		#createZip(save_path,str(save_path+"/asd.zip"))
		zipMe(str(save_path+"/T_"+str(dbTorrent.id)+".zip"),str(save_path+"/*"))
		dbTorrent.Status = "Processing"
		dbTorrent.Progress = str(100);
		dbTorrent.Status = "Complete"
		dbTorrent.Progress = str(100);
		dbTorrent.save()
		ses.remove_torrent(h);
		print h.name(), 'complete'
		return ''