# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Torrents'
        db.create_table('torrents_torrents', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(default='No MetaData', max_length=200)),
            ('Magnet', self.gf('django.db.models.fields.CharField')(max_length=2000, null=True, blank=True)),
            ('TorrentFile', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('Status', self.gf('django.db.models.fields.CharField')(default='Just Added', max_length=100, null=True, blank=True)),
            ('Progress', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal('torrents', ['Torrents'])


    def backwards(self, orm):
        # Deleting model 'Torrents'
        db.delete_table('torrents_torrents')


    models = {
        'torrents.torrents': {
            'Magnet': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'Torrents'},
            'Name': ('django.db.models.fields.CharField', [], {'default': "'No MetaData'", 'max_length': '200'}),
            'Progress': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'Status': ('django.db.models.fields.CharField', [], {'default': "'Just Added'", 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'TorrentFile': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['torrents']