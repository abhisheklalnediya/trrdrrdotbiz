from django.shortcuts import render_to_response
from django.template import RequestContext
from django.forms.formsets import formset_factory
from django.http import HttpResponse
from django import http
from django.db.models import get_app, get_models
from django.db.models import get_model
import random
from django import forms
from trrdrrdotbiz.torrents.ltFunctions import * ;
from trrdrrdotbiz.torrents.models import * ;
import re
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.forms.util import ErrorList
from django.contrib import auth

def userRegister(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		emailfiled = forms.EmailField()
		form.is_valid()
		try :
			emailfiled.clean(request.POST['username'])
			if form.is_valid():
				new_user = form.save()
				return HttpResponseRedirect("/")
		except Exception as e :
			errors = form._errors.setdefault("username", ErrorList())
			errors.append(u"Invalid Email")
	else:
		form = UserCreationForm()
	return render(request, "register.html", {'form': form,}, context_instance=RequestContext(request))



def login_view(request):
	if request.method == 'POST':
		username = request.POST.get('username', '')
		password = request.POST.get('password', '')
		user = auth.authenticate(username=username, password=password)
		if user is not None and user.is_active:
			# Correct password, and the user is marked "active"
			auth.login(request, user)
			# Redirect to a success page.
			return HttpResponseRedirect("/dashboard")
		else:
			return render_to_response("login.html", {'loginerror' :'Invalid email or password :('}, context_instance=RequestContext(request))
	else:
		return render_to_response("login.html", context_instance=RequestContext(request))
	
def logout_view(request):
	auth.logout(request)
	# Redirect to a success page.
	return HttpResponseRedirect("/letmeIn")

def DashHome(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect("/letmeIn")
	return render_to_response("addnew.html", context_instance=RequestContext(request))

def home(request):
	return render_to_response("home.html", context_instance=RequestContext(request))

def addTrr(request):
	if request.method == 'POST':
		if request.user.is_authenticated():
			print request.user
			
			if 'url' in request.POST and request.POST['url'] != '':
				print request.POST['url'];
				MagnetUrl = request.POST['url'];
				
				getUrlType(request.POST['url']);
				
				TorrentThread = threading.Thread(target=addTorrentMagnet,args=[MagnetUrl,request])
				TorrentThread.start()		
				TorrentThreads.append(TorrentThread,)
			return render_to_response("addnew.html", context_instance=RequestContext(request))
		else:
			return http.HttpResponse('No User Logged In')
		
			#return http.HttpResponseServerError('<h1>Auth Error</h1>')
	return http.HttpResponseServerError('<h1>Server Error (500)</h1>')

def delTorrent(request):
	if request.user.is_authenticated():
		if 'pk' in request.POST:
			tr=Torrents.objects.get(pk=request.POST['pk'])
			tr.Status = 'deleted'
			tr.save()
			return http.HttpResponse('Deleted')
		return http.HttpResponseServerError('<h1>Server Error (500)</h1>')
	else:
		return HttpResponseRedirect("/letmeIn")

def myTrrs(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect("/letmeIn")
	torrents = Torrents.objects.filter().exclude(Status = 'deleted').exclude(Status = 'Complete')
	return render_to_response("myTrrs.html",{'torrents':torrents}, context_instance=RequestContext(request))

def myDrrs(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect("/letmeIn")
	torrents = Torrents.objects.filter(Status = 'Complete');
	return render_to_response("myDrrs.html",{'torrents':torrents}, context_instance=RequestContext(request))

def getUrlType(url):
	if re.match('^http.+', url):
		print 'asasdsad'