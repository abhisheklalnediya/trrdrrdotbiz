from django.conf.urls import patterns, include, url
from dashboard.views import *
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^dashboard$',DashHome),
    
    url(r'^addTrr$',addTrr),
    url(r'^delTorrent$',delTorrent),
    
    url(r'^myTrrs$',myTrrs),
    url(r'^myDrrs$',myDrrs),
    
    url(r'^$',home),
    
    
    #User
    
    url(r'^register$',userRegister),
    url(r'^letmeIn$',login_view),
    url(r'^letmeOut$',logout_view),
    # url(r'^$', 'trrdrrdotbiz.views.home', name='home'),
    # url(r'^trrdrrdotbiz/', include('trrdrrdotbiz.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
